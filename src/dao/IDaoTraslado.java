/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.IOException;
import pojo.Traslado;

/**
 *
 * @author Yasser
 */
public interface IDaoTraslado extends IDao<Traslado> {
    Traslado buscarPorId(int id) throws IOException;
}
